import React from 'react'; /* import React and tags for TabBar, along with icons. Also, include css. */
import { IonTabs,  IonTabBar, IonTabButton, IonIcon, IonLabel, IonBadge, IonRouterOutlet } from '@ionic/react';
import { home, bag, scan, cart, restaurantOutline } from 'ionicons/icons';
import './Home.css';

/* this containes our app */
const Home: React.FC = () => {
  return (
    <IonTabs>
      <IonTabBar slot="bottom">
        <IonTabButton tab="home" href="/home">
          <IonIcon icon={home} />
          <IonLabel>Home</IonLabel> 
        </IonTabButton>

        <IonTabButton tab="inventory" href="/inventory">
          <IonIcon icon={bag}/>
          <IonLabel>Inventory</IonLabel>
        </IonTabButton>

        <IonTabButton tab="scanning" href="/scanning">
          <IonIcon icon={scan}/>
          <IonLabel>Scanning</IonLabel>
        </IonTabButton>

        <IonTabButton tab="shopping" href="/shopping">
          <IonIcon icon={cart}/>
          <IonLabel>Shopping</IonLabel>
        </IonTabButton>

        <IonTabButton tab="recipes" href="/recipes">
          <IonIcon icon={restaurantOutline}/>
          <IonLabel>Recipes</IonLabel>
        </IonTabButton>

      </IonTabBar>
      <IonRouterOutlet></IonRouterOutlet>
    </IonTabs>    
    
  );
};

export default Home;
