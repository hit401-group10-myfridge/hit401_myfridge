import React from 'react'; /* import React and tags for TabBar, along with icons. Also, include css. */
import { IonButton, IonRow, IonContent, IonHeader, IonToolbar, IonTitle, IonIcon } from '@ionic/react';
import { add, star, alert, arrowForward } from 'ionicons/icons';
import './Home';

const Recipe: React.FC = () => {
    return (
<IonContent>
    <br></br>
    <IonToolbar>
        <IonTitle>
            Recipe            
        </IonTitle>        
    </IonToolbar>  
    <br></br><br></br><br></br>

    <IonButton color="dark" expand="full" fill="outline" size="large">
    <IonIcon icon={alert}/>  
        Suggest Recipe
    </IonButton>
    <br></br><br></br>

    <IonButton color="dark" expand="full" fill="outline" size="large">
        <IonIcon icon={add}/>
         Add Recipe
    </IonButton>
    <br></br><br></br>

    <IonButton color="dark" expand="full" fill="outline" size="large">
    <IonIcon icon={star} class="absolute-icon" />
        My Recipe
    </IonButton>
    <br></br><br></br><br></br><br></br>
    <br></br>

    <IonButton expand="block" color="medium" fill="solid" size="default"> 
        Log Out
    </IonButton>
    


 </IonContent>
    );
  };
  
  export default Recipe;
  